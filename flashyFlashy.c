/*! \file  flashyFlashy.c
 *
 *  \brief
 *
 *
 *  \author jjmcd
 *  \date July 17, 2015, 3:11 PM
 *
 * Software License Agreement
 * Copyright (c) 2015 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */
#include <xc.h>
#include "../include/graphicsHostLibrary.h"

/*! flashyFlashy - */

/*!
 *
 */
/* Flash the LED a bunch of times so we know when things start */
void flashyFlashy( void )
{
  int i;

  // LED flashing lets me know when the application actually
  // starts sending data.  LED is on a second, flashed quickly
  // 5 times, stays on a second, goes off a second, then data starts

  _LATB9 = 1;
  delay(1000);

  for (i = 0; i < 5; i++)
    {
      _LATB9 = 0;
      delay(100);
      _LATB9 = 1;
      delay(100);
    }
  delay(1000);

  _LATB9 = 0;
  delay(1000);
}
