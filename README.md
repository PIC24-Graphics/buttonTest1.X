## Simple button test

Creates a button with TFTBUTMAKE1

If the consant AAA is defined, the button is tested with TFTgetTouch()  
If not, it is tested with TFTBUTSTATE

A circle is DEEPPINK if the button is touched, DARKRED if not.
