/*! \file  buttonTest1.c
 *
 *  \brief
 *
 *
 *  \author jjmcd
 *  \date July 17, 2015, 3:08 PM
 *
 * Software License Agreement
 * Copyright (c) 2015 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */
#include <xc.h>
#include <string.h>
#include "../include/graphicsHostLibrary.h"
#include "../include/colors.h"

/* Configuration fuses */
#pragma config FNOSC = FRCPLL       // Fast RC oscillator with PLL
#pragma config FWDTEN = OFF         // Turn off watchdog timer

#define BACKGROUNDCOLOR BURLYWOOD
void flashyFlashy();

/* Define for touch, undefine for button */
#define AAA

char legend[32];

/*! main - */

/*!
 *
 */
int main(void)
{
  int i;
  unsigned char state;
  unsigned int x,y;

  // LED will be used for general signaling
  _TRISB9 = 0;

  serialInitialize(0);

  flashyFlashy();

  TFTinit(TFTLANDSCAPE);
  TFTsetBackColorX(BACKGROUNDCOLOR);
  TFTclear();

  memset(legend,0,sizeof(legend));
  putch(TFTBUTMAKE1);
  putch(4);         // Button ID
  sendWord(100);    // x1
  sendWord(100);    // y1
  sendWord(220);    // x2
  sendWord(140);    // y2
  strcpy(legend,"Press Here");
  for ( i=0; i<24; i++ )
    putch(legend[i]);

  while(1)
    {
#ifdef AAA
      state = 0x00;
      /* Check to see whether there is touch data */
      if (TFTgetTouch(&x, &y))
        if ( x>100 )
          if ( y>100 )
            if ( x<220 )
              if ( y<140 )
                state = 0x80;
#else
      putch(TFTBUTSTATE);
      putch(4);
      state=getch();
#endif
      if ( state & 0x80 )
        {
          TFTsetColorX(DEEPPINK);
        }
      else
        {
          TFTsetColorX(DARKRED);
        }
      TFTfillCircle(160,40,20);
      delay(50);
    }

  return 0;
}
